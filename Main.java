package de.heliosdevelopment.statsapi;

import de.heliosdevelopment.statsapi.util.SQLInfo;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        if (!getConfig().contains("host"))
            getConfig().set("host", "localhost");
        if (!getConfig().contains("port"))
            getConfig().set("port", 3306);
        if (!getConfig().contains("database"))
            getConfig().set("database", "database");
        if (!getConfig().contains("username"))
            getConfig().set("username", "username");
        if (!getConfig().contains("password"))
            getConfig().set("password", "password");
        saveConfig();
        try {
            SQLClient client = new SQLClient(new SQLInfo(getConfig().getString("host"), getConfig().getInt("port"), getConfig().getString("database"), getConfig().getString("username"), getConfig().getString("password")), "com.mysql.jdbc.Driver", "jdbc:mysql", 5);
            StatsAPI statsAPI = new StatsAPI(client);
            if (!statsAPI.bootstrap()) {
                client.doShutdown();
                getServer().getPluginManager().disablePlugin(this);
            }
            Bukkit.getPluginManager().registerEvents(new PlayerListener(statsAPI), this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        StatsAPI.getInstance().getSqlClient().doShutdown();
    }


}

package de.heliosdevelopment.statsapi;

import de.heliosdevelopment.statsapi.util.StatsPlayer;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StatsAPI {

    private final SQLClient sqlClient;
    private static StatsAPI instance;

    public StatsAPI(SQLClient sqlClient) {
        this.sqlClient = sqlClient;
        this.instance = this;
    }

    public SQLClient getSqlClient() {
        return sqlClient;
    }

    public static StatsAPI getInstance() {
        return instance;
    }

    public boolean bootstrap() {
        try (Connection connection = sqlClient.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Stats(name VARCHAR(99), uuid VARCHAR(99), kills INT(10), deaths INT(10), PRIMARY KEY (uuid));");
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    private final List<StatsPlayer> playerList = new ArrayList<>();

    public void loadPlayer(Player player) {
        int[] stats = getStats(player.getUniqueId());
        StatsPlayer statsPlayer;
        if (stats[0] == -1) {
            addStats(player.getName(), player.getUniqueId());
            statsPlayer = new StatsPlayer(player.getUniqueId(), player.getName(), 0, 0);
        } else {
            statsPlayer = new StatsPlayer(player.getUniqueId(), player.getName(), stats[0], stats[1]);
        }
        playerList.add(statsPlayer);
    }

    public void unloadPlayer(Player player) {
        StatsPlayer statsPlayer = getPlayer(player);
        if (statsPlayer != null) {
            setStats(statsPlayer);
            playerList.remove(statsPlayer);
        }
    }

    public StatsPlayer getPlayer(Player player) {
        for (StatsPlayer statsPlayer : playerList)
            if (statsPlayer.getUuid().equals(player.getUniqueId()))
                return statsPlayer;
        return null;
    }

    public StatsPlayer getPlayer(UUID uuid) {
        for (StatsPlayer statsPlayer : playerList)
            if (statsPlayer.getUuid().equals(uuid))
                return statsPlayer;

        return getStatsPlayer(uuid);
    }

    public StatsPlayer getPlayer(String name) {
        for (StatsPlayer statsPlayer : playerList)
            if (statsPlayer.getName().equalsIgnoreCase(name))
                return statsPlayer;

        return getStatsPlayer(name);
    }

    public void addKill(UUID uuid) {
        StatsPlayer statsPlayer = getPlayer(uuid);
        if (statsPlayer != null)
            statsPlayer.setKills(statsPlayer.getKills() + 1);
    }

    public void addDeath(UUID uuid) {
        StatsPlayer statsPlayer = getPlayer(uuid);
        if (statsPlayer != null)
            statsPlayer.setDeaths(statsPlayer.getDeaths() + 1);
    }

    public int getKills(UUID uuid) {
        StatsPlayer statsPlayer = getPlayer(uuid);
        if (statsPlayer != null)
            return statsPlayer.getKills();
        return 0;
    }

    public int getDeaths(UUID uuid) {
        StatsPlayer statsPlayer = getPlayer(uuid);
        if (statsPlayer != null)
            return statsPlayer.getDeaths();
        return 0;
    }

    public double getKD(UUID uuid) {
        StatsPlayer statsPlayer = getPlayer(uuid);
        if (statsPlayer != null) {
            if (statsPlayer.getDeaths() == 0)
                return statsPlayer.getKills();
            else if (statsPlayer.getKills() == 0)
                return 0;
            else
                return (double) (statsPlayer.getKills() / statsPlayer.getDeaths());
        }
        return 0;
    }

    public int getRank(UUID uuid) {
        StatsPlayer statsPlayer = getPlayer(uuid);
        if (statsPlayer != null) {
            try (Connection connection = sqlClient.getConnection()) {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT kills from Stats WHERE kills >= ?");

                preparedStatement.setInt(1, statsPlayer.getKills());
                ResultSet resultSet = preparedStatement.executeQuery();
                int rank = 1;
                while (resultSet.next()) {
                    rank++;
                }
                return rank;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    private int[] getStats(UUID uuid) {
        try (Connection connection = sqlClient.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Stats WHERE `uuid`=?");

            preparedStatement.setString(1, uuid.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new int[]{resultSet.getInt("kills"), resultSet.getInt("deaths")};
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new int[]{-1, -1};
    }

    private void setStats(StatsPlayer statsPlayer) {
        try (Connection connection = sqlClient.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Stats SET kills=?, deaths=? WHERE uuid=?");

            preparedStatement.setInt(1, statsPlayer.getKills());
            preparedStatement.setInt(2, statsPlayer.getDeaths());
            preparedStatement.setString(3, statsPlayer.getUuid().toString());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addStats(String name, UUID uuid) {
        try (Connection connection = sqlClient.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Stats (name, uuid, kills, deaths) VALUES (?,?,?,?)");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, uuid.toString());
            preparedStatement.setInt(3, 0);
            preparedStatement.setInt(4, 0);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private StatsPlayer getStatsPlayer(UUID uuid) {
        try (Connection connection = sqlClient.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Stats WHERE `uuid`=?");

            preparedStatement.setString(1, uuid.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return new StatsPlayer(uuid, resultSet.getString("name"), resultSet.getInt("kills"), resultSet.getInt("deaths"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private StatsPlayer getStatsPlayer(String name) {
        try (Connection connection = sqlClient.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Stats WHERE `name`=?");

            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return new StatsPlayer(UUID.fromString(resultSet.getString("uuid")), name, resultSet.getInt("kills"), resultSet.getInt("deaths"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}

package de.heliosdevelopment.statsapi.util;

public interface Runnabled<E> {

    void run(E value);

}
package de.heliosdevelopment.statsapi.util;

public class SQLInfo {

    private String hostName;
    private int port;
    private String database;
    private String userName;
    private String password;

    public SQLInfo(String hostName, int port, String database, String userName, String password) {
        this.hostName = hostName;
        this.port = port;
        this.database = database;
        this.userName = userName;
        this.password = password;
    }

    public String getHostName() {
        return hostName;
    }

    public int getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
package de.heliosdevelopment.statsapi;

import de.heliosdevelopment.statsapi.util.Runnabled;
import de.heliosdevelopment.statsapi.util.SQLInfo;
import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLClient {

    protected final Logger logger = Logger.getLogger("SQLClient");
    protected final BasicDataSource basicDataSource = new BasicDataSource();

    public SQLClient(SQLInfo sqlInfo, String clazz, String prefix, int maxConnections, Runnabled<BasicDataSource>... values) throws SQLException {
        this.basicDataSource.setDriverClassName(clazz);
        this.basicDataSource.setUsername(sqlInfo.getUserName());
        this.basicDataSource.setPassword(sqlInfo.getPassword());
        this.basicDataSource.setUrl(prefix + "://" + sqlInfo.getHostName() + ":" + sqlInfo.getPort() + "/" + sqlInfo.getDatabase() + "?autoReconnect=true");

        this.basicDataSource.setValidationQueryTimeout(4000);
        this.basicDataSource.setMaxIdle(maxConnections);
        this.basicDataSource.setTimeBetweenEvictionRunsMillis(1000 * 60 * 15);

        for (Runnabled<BasicDataSource> basicDataSourceRunnabled : values) {
            basicDataSourceRunnabled.run(basicDataSource);
        }
    }

    public Connection getConnection() {
        try {
            return basicDataSource.getConnection();
        } catch (SQLException e) {
            this.logger.log(Level.WARNING, "Error on returns a connection", e);
            return null;
        }
    }

    public SQLClient doShutdown() {
        try {
            basicDataSource.close();
        } catch (SQLException e) {
            logger.log(Level.WARNING, "Error on closing", e);
        }
        return this;
    }

}
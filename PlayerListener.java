package de.heliosdevelopment.statsapi;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    private final StatsAPI statsAPI;

    public PlayerListener(StatsAPI statsAPI) {
        this.statsAPI = statsAPI;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        statsAPI.loadPlayer(event.getPlayer());
        statsAPI.addKill(event.getPlayer().getUniqueId());
        statsAPI.addKill(event.getPlayer().getUniqueId());
        statsAPI.addDeath(event.getPlayer().getUniqueId());
        System.out.println("KD: "+statsAPI.getKD(event.getPlayer().getUniqueId()));
        System.out.println("RANK: "+statsAPI.getRank(event.getPlayer().getUniqueId()));
        System.out.println("KILLS: "+statsAPI.getKills(event.getPlayer().getUniqueId()));
        System.out.println("DEATHS: "+statsAPI.getDeaths(event.getPlayer().getUniqueId()));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        statsAPI.unloadPlayer(event.getPlayer());
    }
}
